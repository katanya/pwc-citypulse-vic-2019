// Copyright © 2018 Katanya LLP (UK) [www.katanya.co.uk].  All rights reserved. //
var isMobile = false;
var isTouch = false;
var featureCount = 0;
var minValue = 1;
var maxValue = 4;
var average_values = {
    'live': '5.2',
    'work': '4.1',
    'play': '4.4'
}
var regional_values = {
    'live': '4.6',
    'work': '4.8',
    'play': '3.5'
}
var custom_averages = {
    'live': '0',
    'work': '0',
    'play': '0'
}

var map, popup, Popup;
var mapType = 'live';
var searching = false;
var searchSelect = '';
var timerCleared = false;
var sliderTimer;

var colourScale = ['#fff0e8', '#fedece', '#fed0b8', '#fec5a7', '#feb994', '#feab7f', '#fea271', '#fd9760', '#fd8748', '#fd742a', '#fd6412']
var customScale = ['#fee7eb', '#fbd8de', '#f8cbd3', '#f5bfc9', '#f2b4bf', '#efa6b2', '#ec99a7', '#e88b9b', '#e57e90', '#e27184', '#df6479']

var regional = false;
var regionalZones = ["Ballarat", "Loddon", "Avoca", "Maryborough Region", "Maryborough (Vic.)", "Castlemaine Region", "Castlemaine", "Bendigo Region - South", "Kangaroo Flat - Golden Square", "Maiden Gully", "Flora Hill - Spring Gully", "Strathfieldsaye", "East Bendigo - Kennington", "Bendigo", "California Gully - Eaglehawk", "Bendigo Region - North", "White Hills - Ascot", "Heathcote", "Kyneton", "Daylesford", "Creswick - Clunes", "Beaufort", "Golden Plains - North", "Smythes Creek", "Ballarat - South", "Delacombe", "Buninyong", "Alfredton", "Wendouree - Miners Rest", "Ballarat - North", "Gordon (Vic.)", "Bacchus Marsh Region", "Golden Plains - South", "Winchelsea", "Lorne - Anglesea", "Torquay", "Ocean Grove - Barwon Heads", "Point Lonsdale - Queenscliff", "Clifton Springs", "Portarlington", "Leopold", "Newcomb - Moolap", "3Geelong", "Geelong West - Hamlyn Heights", "Geelong", "Belmont", "Highton", "Grovedale", "North Geelong - Bell Park", "Corio - Norlane", "Bannockburn", "Lara", "Woodend", "Newtown (Vic.)"]
var allRegionalZones = [];

var infowindow;
var info_open = false;
var info_title;
var info_values = [];

// Variables related to the map
var live_centers = [];
var work_centers = [];
var play_centers = [];
var markers = [];
var suburb_list = [];
var filtered_suburbs = [];

// Customise map variables
var qs_answered = [];
var custom_complete = false;
var play_vals = {
    "beach-slider": [1024, 2048, 4096],
    "outdoor-slider": [8192, 16384, 32768],
    "culture-slider": [65536, 131072, 262144],
    "bars-slider": [524288, 1048576, 2097152],
    "retail-slider": [4194304, 8388608, 16777216],
    "sport-slider": [33554432, 67108864, 134217728],
}
var dflt = [
    [0.3500, 1.00, 1.00, 0, 5.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.1500, 1.00, 1.00, 0, 5.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0.8000, 1.00, 0, 0, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.2000, 1.00, 0, 0, 0, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0.2000, 1.00, 0, 10.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.8000, 1.00, 0, 10.00, 0, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0.1500, 1.00, 0, 0, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.1500, 1.00, 0, 0, 0, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0.1200, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1200, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1200, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1200, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1200, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1200, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 3.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 3.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.2000, 1.00, 1.00, 1.00, 3.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0.8000, 1.00, 1.00, 1.00, 3.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.8000, 1.00, 1.00, 1.00, 3.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 0, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 0, 3.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 3.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [1.0000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 0, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 2.00, 0, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 2.00, 0, 0, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 0, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00],
    [0.1400, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00],
    [0.1400, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00],
    [0.1400, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00],
    [0.1400, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.7000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00],
    [0.3000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00],
    [0.1050, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 0, 1.00, 5.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1050, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 0, 1.00, 5.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1050, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 5.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1050, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1050, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1050, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1050, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
];

var reg_dflt = [
    [0.1000, 1.00, 1.00, 0, 5.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.1500, 1.00, 1.00, 0, 5.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0.2000, 1.00, 0, 0, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.2000, 1.00, 0, 0, 0, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0.0500, 1.00, 0, 10.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.8000, 1.00, 0, 10.00, 0, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0.0500, 1.00, 0, 0, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.8000, 1.00, 0, 0, 0, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [1.0000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.5000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [1.0000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [1.0000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [1.0000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.5000, 1.00, 1.00, 1.00, 3.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 3.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.2000, 1.00, 1.00, 1.00, 3.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 3.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
    [0.5000, 1.00, 1.00, 1.00, 3.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00],
    [0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [1.0000, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [1.0000, 1.00, 1.00, 0, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.5000, 1.00, 1.00, 0, 3.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [1.0000, 1.00, 1.00, 3.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [1.0000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.7000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.7000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.7000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 0, 1.00, 1.00],
    [0.2500, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 2.00, 0, 1.00, 1.00],
    [0.2500, 1.00, 1.00, 1.00, 1.00, 1.00, 2.00, 0, 0, 1.00, 1.00],
    [0.2500, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 0, 1.00, 1.00],
    [1.0000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00],
    [1.0000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00],
    [0.1750, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00],
    [0.5250, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00],
    [0.1750, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00],
    [0.5250, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00],
    [0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.5000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.3000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.3000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.2000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00],
    [0.3000, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00],
    [0.0700, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 0, 1.00, 5.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1050, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 0, 1.00, 5.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.3500, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 5.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1400, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.0700, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1400, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00],
    [0.1400, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 1.00, 1.00, 1.00, 0, 1.00, 2.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00]
];

$(document).ready(function () {
    // JQUERYUI FUNCTIONS
    $(function () {
        $("#slider-range").slider({
            range: true,
            min: 1,
            max: 4,
            step: 1,
            values: [1, 4],
            slide: function (event, ui) {
                minValue = ui.values[0];
                maxValue = ui.values[1];
            }
        }).each(function () {
            var opt = $(this).data().uiSlider.options;
            var vals = opt.max - opt.min;
            for (var i = 0; i <= vals; i++) {
                var count = -1;
                var text = '';
                while (count != i) {
                    text = text + '$';
                    count++;
                }
                var el = $('<label>' + text + '</label>').css('left', (i / vals * 100) + '%');
                $(".slider-labels").append(el);
            }
        });
    });

    // Customise activity sliders
    $('.q-slider').each(function () {
        $(this).slider({
            min: 0,
            max: 2,
            step: 1,
            value: 1
        });
    });

    // Open and close the custom tab
    $('.tab-heading').on('click', function (e) {
        if ($("#qs-tab")[0].classList.contains('custom-on')) {
            $("#qs-tab").toggleClass('showTab');
            if ($("#qs-tab")[0].classList.contains('showTab')) {
                $('#key-box').addClass('hide');
            }
            if (isTouch && infowindow.getMap() != null) {
                infowindow.close();
            }
        }
    });
    $('.tab-heading .custom-switch').on('click', function (event) {
        event.stopPropagation();
    });
});

$(window).on('load resize', function () {
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
        isTouch = true;
        if ($(window).width() < 650) {
            isMobile = true;
        } else {
            isMobile = false;
        }
    } else {
        isMobile = false;
        isTouch = false;
    }
}).resize();

$(window).on('load', function() {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
});

$(document).bind('mousemove', function (e) {
    $('.popup-tip-anchor').offset({
        left: e.pageX + 20,
        top: e.pageY + 20
    });
});


// ----------------------- FUNCTIONS ----------------------- //
function selectColour(score) {
    if (score === "N/A" || isNaN(score)) {
        return "#ffffff";
    } else {
        var percentile = Math.floor(score);
        if ($('#qs-tab')[0].classList.contains('custom-on')) {
            return customScale[percentile];
        } else {
            return colourScale[percentile];
        }
    }
}

function styleGEOJson(dtype) {
    mapType = dtype;

    map.data.setStyle(function (feature) {
        var propertyPrice = feature.getProperty('property_price');
        var featureName = feature.getProperty('name');
        if (allRegionalZones.indexOf(featureName) > -1 && regional || allRegionalZones.indexOf(featureName) < 0 && !regional) {
            if ((propertyPrice >= minValue && propertyPrice <= maxValue) || ($('#ins-data')[0].checked && propertyPrice == 0)) {
                if (custom_complete && $('#qs-tab')[0].classList.contains('custom-on')) {
                    var color = selectColour(feature.getProperty("c_" + mapType));
                } else {
                    var color = selectColour(parseFloat(feature.getProperty(mapType)));
                }
                return {
                    fillColor: color,
                    fillOpacity: 0.5,
                    strokeWeight: 0.5,
                    strokeColor: '#6D6E71'
                };
            } else {
                return {
                    fillOpacity: 0,
                    strokeWeight: 0,
                }
            }
        } else {
            return {
                fillOpacity: 0,
                strokeWeight: 0,
            }
        }
    });
}

function addSuburbs(suburb, sa2, price, postcode) {
    var suburbs = suburb.split(";");
    var postcodes = postcode.toString().split(";");
    for (var item in suburbs) {
        if (suburbs[item].length > 0) {
            if (filtered_suburbs.indexOf(suburbs[item]) < 0) {
                filtered_suburbs.push(suburbs[item]);
                var sb_key = suburbs[item];
                var data = {
                    label: sb_key,
                    type: 'Suburb',
                    desc: 'Suburb',
                    pcode: postcodes[item],
                    pp: price
                }
                var pdata = {
                    label: postcodes[item],
                    type: 'Postcode',
                    suburb: sb_key,
                    desc: 'Suburb',
                    pcode: postcodes[item],
                    pp: price
                }
                suburb_list.push(data);
                suburb_list.push(pdata);
            }
        }
    }
    if (filtered_suburbs.indexOf(sa2) < 0) {
        filtered_suburbs.push(sa2);
        var data = {
            label: sa2,
            type: 'suburb',
            desc: 'Statistical Area 2 (ABS)',
            pcode: postcode,
            pp: price
        }
        suburb_list.push(data);
    }
    suburb_list.sort();
}

function getPolygonCenters(event, mapType, list) {
    if (event.feature.getProperty(mapType) > 7) {
        var marker_bounds = new google.maps.LatLngBounds();
        event.feature.getGeometry().forEachLatLng(function (latlng) {
            marker_bounds.extend(latlng);
        });
        list.push(marker_bounds);
    }
}

function getShadow() {
    if ($('.custom-switch')[0].checked) {
        return 'c-shadowBack';
    } else {
        return 'shadowBack';
    }
}

var styles = {
    default: null,
    hide: [
        {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.neighborhood",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.local",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        }
    ]
};

function initMap() {
    var normalBounds = new google.maps.LatLngBounds();
    var regionalBounds = new google.maps.LatLngBounds();
    // Setup the map and related features
    if (isTouch) {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            mapTypeControl: false,
            streetViewControl: false,
            zoomControl: false,
            fullscreenControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            },
            center: {
                lat: -37.8,
                lng: 144.9
            }
        });
    } else {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            mapTypeControl: false,
            streetViewControl: false,
            fullscreenControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            },
            center: {
                lat: -37.8,
                lng: 144.9
            }
        });
    }
    map.setOptions({ styles: styles['hide'] });
    map.data.loadGeoJson('city-pulse-vic-2019.json');
    if (isTouch) {
        infowindow = new google.maps.InfoWindow({
            pixelOffset: new google.maps.Size(0, -10)
        });

        google.maps.event.addListener(infowindow, 'closeclick', function () {
            map.data.revertStyle();
        });
    }
    styleGEOJson(mapType);
    $('#' + mapType).addClass('shadowBack');

    map.data.addListener('addfeature', function (event) {
        featureCount++;
        event.feature.setProperty('id', featureCount);
        var featureName = event.feature.getProperty('name');
        var sub = event.feature.getProperty('suburbs').split(";");
        if (regionalZones.indexOf(featureName) > -1) {
            allRegionalZones.push(featureName);
            for (var i = 0; i < sub.length; i++) {
                if (allRegionalZones.indexOf(sub[i]) < 0) {
                    allRegionalZones.push(sub[i]);
                }
            }
            processPoints(event.feature.getGeometry(), regionalBounds.extend, regionalBounds);
        } else {
            processPoints(event.feature.getGeometry(), normalBounds.extend, normalBounds);
        }
        getPolygonCenters(event, 'live', live_centers);
        getPolygonCenters(event, 'work', work_centers);
        getPolygonCenters(event, 'play', play_centers);
        addSuburbs(event.feature.getProperty('suburbs'), event.feature.getProperty('name'), event.feature.getProperty('property_price'), event.feature.getProperty('postcodes'));
        if (!isMobile) {
            if (regional) {
                map.fitBounds(regionalBounds);
            } else {
                map.fitBounds(normalBounds);
            }
        }
    });

    definePopupClass();
    popup = new Popup(
        new google.maps.LatLng(-33, 151),
        document.getElementById('content'));
    popup.setMap(map);

    var overlay = new google.maps.OverlayView();
    overlay.draw = function () { };
    overlay.setMap(map);

    // Add controls to the map
    var topDiv = document.getElementById('top-box');
    var tabControl = document.getElementById('qs-tab');
    var propertyControl = document.getElementById('property-slider');
    var keyDiv = document.getElementById('key-box');

    if (isMobile) {
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(topDiv);
        map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(propertyControl);
        map.controls[google.maps.ControlPosition.LEFT].push(tabControl);
        map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(keyDiv);

        var keyTimer = setTimeout(function () { $('#key-box').addClass('hide'); timerCleared = true; }, 5000);
    } else {
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(topDiv);
        map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(propertyControl);
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(tabControl);
        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(keyDiv);
    }

    // ------------------- FUNCTIONS ----------------------------- //
    function clearMap() {
        styleGEOJson(mapType);
        if (isMobile || regional) {
            map.setZoom(8);
        }
    }

    $('.key-tab').on('click', function (e) {
        $('#key-box').toggleClass('hide');
        if (!timerCleared) { console.log('cleared'); clearTimeout(keyTimer) };
    });

    function getSearchItems(searchTerm, click, customTab) {
        if (!click) {
            searchSelect = '';
        }
        if (isTouch) { 
            infowindow.close();
            map.data.revertStyle();
        }
        if(!customTab){
            $('#qs-tab').removeClass('showTab');
        }
        $('#qs-tab').removeClass('showTab');
        $('#key-box').addClass('hide');
        if (searchTerm === "") {
            styleGEOJson(mapType);
            document.getElementById('search-close').classList.add('hidden');
        } else {
            var search_bounds = new google.maps.LatLngBounds();
            var searchAreas = [];
            var searchFound = false;
            map.data.forEach(function (feature) {
                var propertyPrice = feature.getProperty('property_price');
                var featureSuburbs = feature.getProperty('suburbs').toLowerCase().split(';');
                var featurePostcodes = feature.getProperty('postcodes').toString().split(';');
                var featureName = feature.getProperty('name');
                if (allRegionalZones.indexOf(featureName) > -1 && regional || allRegionalZones.indexOf(featureName) < 0 && !regional) {
                    if ((propertyPrice >= minValue && propertyPrice <= maxValue) || ($('#ins-data')[0].checked && propertyPrice == 0)) {
                        if (feature.getProperty('name').toLowerCase() === searchTerm || feature.getProperty('name').toLowerCase() === searchSelect) {
                            searchAreas.push(feature.getProperty('name').toLowerCase());
                        } else {
                            if (featureSuburbs.length > 0 && searchAreas.length == 0) {
                                for (var item in featureSuburbs) {
                                    if (featureSuburbs[item] === searchTerm) {
                                        searchAreas.push(feature.getProperty('name').toLowerCase());
                                    }
                                }
                            }
                            if (featurePostcodes.length > 0 && searchAreas.length == 0) {
                                for (var item in featurePostcodes) {
                                    if (featurePostcodes[item] === searchTerm) {
                                        searchAreas.push(feature.getProperty('name').toLowerCase());
                                    }
                                }
                            }
                        }
                    }
                }
            });
            map.data.forEach(function (feature) {
                var propertyPrice = feature.getProperty('property_price');
                var featureName = feature.getProperty('name');
                if (allRegionalZones.indexOf(featureName) > -1 && regional || allRegionalZones.indexOf(featureName) < 0 && !regional) {
                    if ((propertyPrice >= minValue && propertyPrice <= maxValue) || ($('#ins-data')[0].checked && propertyPrice == 0)) {
                        if (!click) {
                            var condition = (feature.getProperty('name').toLowerCase().indexOf(searchTerm) >= 0 || feature.getProperty('suburbs').toLowerCase().split(';').indexOf(searchTerm) >= 0 || feature.getProperty('postcodes').toString().split(';').indexOf(searchTerm) >= 0);
                        } else {
                            var condition = (searchAreas.indexOf(feature.getProperty('name').toLowerCase()) >= 0);
                        }
                        if (condition) {
                            processPoints(feature.getGeometry(), search_bounds.extend, search_bounds);
                            map.fitBounds(search_bounds);
                            searchFound = true;
                        }
                        if (!searchFound) {
                            if (regional) {
                                map.fitBounds(regionalBounds);
                            } else {
                                map.fitBounds(normalBounds);
                            }
                        }
                    }
                }
            });

            map.data.setStyle(function (feature) {
                if (custom_complete && $('#qs-tab')[0].classList.contains('custom-on')) {
                    var color = selectColour(feature.getProperty("c_" + mapType));
                } else {
                    var color = selectColour(parseFloat(feature.getProperty(mapType)));
                }
                var propertyPrice = feature.getProperty('property_price');
                if (!click) {
                    var condition = (feature.getProperty('name').toLowerCase().indexOf(searchTerm) >= 0 || feature.getProperty('suburbs').toLowerCase().split(';').indexOf(searchTerm) >= 0 || feature.getProperty('postcodes').toString().split(';').indexOf(searchTerm) >= 0);
                } else {
                    var condition = (searchAreas.indexOf(feature.getProperty('name').toLowerCase()) >= 0);
                }

                var featureName = feature.getProperty('name');
                if (allRegionalZones.indexOf(featureName) > -1 && regional || allRegionalZones.indexOf(featureName) < 0 && !regional) {
                    if ((propertyPrice >= minValue && propertyPrice <= maxValue) || ($('#ins-data')[0].checked && propertyPrice == 0)) {
                        if (condition) {
                            return {
                                fillColor: color,
                                fillOpacity: 0.5,
                                strokeWeight: 2,
                                strokeColor: '#6D6E71'
                            };
                        } else {
                            return {
                                fillColor: color,
                                fillOpacity: 0.05,
                                strokeWeight: 0.5,
                                strokeColor: '#6D6E71'
                            };
                        }
                    } else {
                        return {
                            fillOpacity: 0,
                            strokeWeight: 0,
                        }
                    }
                } else {
                    return {
                        fillOpacity: 0,
                        strokeWeight: 0,
                    }
                }
            });
        }
    }

    function openInfoBox(location) {
        map.data.forEach(function (feature) {
            var subs = feature.getProperty('suburbs').split(';');
            if (location == feature.getProperty('name') || subs.indexOf(location) > -1) {
                var location_bounds = new google.maps.LatLngBounds();
                processPoints(feature.getGeometry(), location_bounds.extend, location_bounds);
                var html = populateMobilePopup(feature);
                infowindow.setContent(html);
                infowindow.setPosition(location_bounds.getCenter());
                infowindow.open(map);
            }
        });
    }

    function dynamicMapSearch() {
        var searchTerm = $('#search-box').val().toLowerCase();
        if (searchTerm.length > 0) {
            searching = true;
            getSearchItems(searchTerm, false);
        } else if (searchTerm.length == 0) {
            clearMap();
        }
    }

    // ---------------- CUSTOM MAP CALCULATIONS ------------------ //
    function customMapUpdate() {
        var bmask = 1;
        var c_weighting = [];

        $('input:radio[name=lifestage-option]').each(function () {
            if ($(this).is(":checked")) {
                bmask += Number($(this).val());
            }
        });
        $('input:radio[name=tp-work-option]').each(function () {
            if ($(this).is(":checked")) {
                bmask += Number($(this).val());
            }
        });
        $('input:radio[name=tp-owork-option]').each(function () {
            if ($(this).is(":checked")) {
                bmask += Number($(this).val());
            }
        });

        $('.q-slider').each(function () {
            var id = $(this)[0].id;
            var val = $(this).slider("option", "value");
            bmask += Number(play_vals[id][val]);
        });

        var weights;
        if (regional) {
            weights = reg_dflt;
        } else {
            weights = dflt;
        }

        for (var i = 0; i < weights.length; i++) {
            var array = weights[i].slice(1);
            var m = binaryMaskProduct(array, bmask);
            var c = m * weights[i][0];
            c_weighting.push(c);
        }
        m_weights = calculateWeighting(c_weighting);
        addWeightings(m_weights);
    }

    function binaryMaskProduct(array, bitmask) {
        var array2 = array.filter(function (value, index) {
            return bitmask & Math.pow(2, index);
        });
        return array2.reduce(function (a, b) { return a * b; });
    }

    function calculateWeighting(c_weights) {
        var LWP_totals = [];
        var final_weight = [];
        var LWP = [
            [1, 26, "Live"],
            [27, 10, "Work"],
            [37, 25, "Play"]
        ];

        for (var i = 0; i < LWP.length; i++) {
            var start = LWP[i][0];
            var count = LWP[i][1];
            var name = LWP[i][2];
            LWP_totals[name] = 0;
            for (var j = 0; j < count; j++) {
                LWP_totals[name] = LWP_totals[name] + c_weights[start - 1 + j];
            }
        }

        for (var i = 0; i < LWP.length; i++) {
            var start = LWP[i][0];
            var count = LWP[i][1];
            var name = LWP[i][2];
            for (var j = 0; j < count; j++) {
                if (LWP_totals[name] != 0) {
                    final_weight.push(c_weights[start - 1 + j] / LWP_totals[name]);
                } else {
                    final_weight.push(0);
                }
            }
        }
        return final_weight;
    }
    // final weight multiplier
    function addWeightings(multiplier) {
        var avg_count = 0;
        var LWP = [
            [1, 26, "Live"],
            [27, 10, "Work"],
            [37, 25, "Play"]
        ]
        var c_avg = {
            "Live": 0,
            "Work": 0,
            "Play": 0
        }

        map.data.forEach(function (feature) {
            var totals = [];
            var inputs = feature.getProperty("indicator_values").split(';');
            if (inputs.length > 1) {
                avg_count++;
                for (var i = 0; i < LWP.length; i++) {
                    var start = LWP[i][0];
                    var count = LWP[i][1];
                    var name = LWP[i][2];
                    totals[name] = 0;

                    for (var j = 0; j < count; j++) {
                        totals[name] = totals[name] + (inputs[start - 1 + j] * multiplier[start - 1 + j]);
                    }
                }
                c_avg["Live"] = c_avg["Live"] + (totals["Live"] * 10);
                c_avg["Work"] = c_avg["Work"] + (totals["Work"] * 10);
                c_avg["Play"] = c_avg["Play"] + (totals["Play"] * 10);
            }
            feature.setProperty("c_live", totals["Live"] * 10);
            feature.setProperty("c_work", totals["Work"] * 10);
            feature.setProperty("c_play", totals["Play"] * 10);
        });

        for (var key in c_avg) {
            var averaged = (c_avg[key] / avg_count).toFixed(1);
            custom_averages[key.toLowerCase()] = averaged;
        }
        styleGEOJson(mapType);
        var searchTerm = $('#search-box').val().toLowerCase();
        if (searchTerm.length > 1) {
            getSearchItems(searchTerm, !(searching && searchTerm.length > 1), true);
        }
    }

    function custom_clear() {
        map.data.setStyle(function (feature) {
            var featureName = feature.getProperty('name');
            if (allRegionalZones.indexOf(featureName) > -1 && regional || allRegionalZones.indexOf(featureName) < 0 && !regional) {
                var color = selectColour(parseFloat(feature.getProperty(mapType)));
                return {
                    fillColor: color,
                    fillOpacity: 0.05,
                    strokeWeight: 0.5,
                    strokeColor: '#6D6E71'
                };
            } else {
                return {
                    fillOpacity: 0,
                    strokeWeight: 0,
                }
            }
        });
    }

    // ------------------- EVENT LISTENERS ----------------------- //
    // Custom switch
    // On: open custom tab, change colours and text
    // Off: close custom tab, revert colours and text
    $('.custom-switch').on("change", function () {
        gtag('event', 'click', {
            'event_category': 'tab',
            'event_label': 'Customise tab'
        });

        $('#slider-range').toggleClass('original');
        $('.ui-autocomplete').toggleClass('original');
        if (isTouch) {
            map.data.revertStyle();
            infowindow.close();
        }
        if ($(this)[0].checked) {
            $('#key-box').addClass('hide');
            $('.region-btn').addClass('cus');
            $('#buttons-box').addClass('cus');
            $('#qs-tab').toggleClass('custom-on');
            $('#qs-tab').addClass('showTab');
            $('.shadowBack').addClass('c-shadowBack');
            $('.c-shadowBack').removeClass('shadowBack');
            $('#heat-map').addClass('custom');
            $('.avg-values').each(function () {
                $(this)[0].innerText = '0';
            });
            if (custom_complete) {
                customMapUpdate();
            } else {
                custom_clear();
            }
        } else {
            $('.region-btn').removeClass('cus');
            $('#buttons-box').removeClass('cus');
            $('#qs-tab').toggleClass('custom-on');
            $('#qs-tab').removeClass('showTab');
            $('.c-shadowBack').addClass('shadowBack');
            $('.shadowBack').removeClass('c-shadowBack');
            $('#heat-map').removeClass('custom');
            var searchTerm = $('#search-box').val().toLowerCase();
            styleGEOJson(mapType);
            getSearchItems(searchTerm, !(searching && searchTerm.length > 1));
        }
    });


    function updateInfo() {
        var capName = mapType.charAt(0).toUpperCase() + mapType.slice(1);
        var myHTML = "<div id='popup-info'><p style='font-weight:bold;margin: 0px 0px 10px 0px !important;'>";
        myHTML = myHTML + info_title;
        myHTML = myHTML + "</p>";
        value = info_values[mapType];

        if (!isNaN(value)) {
            if (value >= 1) {
                myHTML = myHTML + "<table width='100%'><tbody><tr><td width='50%' style='padding-right:8px'>" + capName + "</td><td width='50%' style='text-align:center;'>" + value + "</td></tr>";
            } else {
                myHTML = myHTML + "<table width='100%'><tbody><tr><td width='50%' style='padding-right:8px'>" + capName + "</td><td width='50%' style='text-align:center;'>&lt;1</td></tr>";
            }
        } else {
            myHTML = myHTML + "<table width='100%'><tbody><tr><td width='100%'>Low population</td></tr>";
        }
        if ($('#qs-tab')[0].classList.contains('custom-on')) {
            myHTML = myHTML + "<tr><td width='75%'>Custom Average</td><td width='25%'>" + custom_averages[mapType] + "</td></tr>"
        } else {
            if (regional) {
                myHTML = myHTML + "<tr><td width='75%'>City Average</td><td width='25%'>" + regional_values[mapType] + "</td></tr>"
            } else {
                myHTML = myHTML + "<tr><td width='75%'>City Average</td><td width='25%'>" + average_values[mapType] + "</td></tr>"
            }
        }
        myHTML = myHTML + "</tbody></table>";

        infowindow.setContent(myHTML);
    }

    // Switching Styles
    $('#buttons-box input').on('click', function () {
        gtag('event', 'click', {
            'event_category': 'tab',
            'event_label': $(this)[0].id + ' tab'
        });

        if (isTouch) {
            var isOpen = (infowindow.getMap() != null);
        }
        var bg_shadow = getShadow();
        var searchTerm = $('#search-box').val().toLowerCase();
        var value = $(this)[0].id
        mapType = value;

        $('#buttons-box input').each(function () {
            $(this).removeClass(bg_shadow);
        });
        $('#' + value).addClass(bg_shadow);

        styleGEOJson(mapType);
        getSearchItems(searchTerm, !(searching && searchTerm.length > 1));
        if (isTouch && info_open && isOpen) {
            updateInfo();
        }
    });

    $('#region-select input').on('click', function () {
        if (regional && $(this).val() != "Regional" || !regional && $(this).val() != "Greater Melbourne") {
            if (isTouch) {infowindow.close()};
            $("#qs-tab").removeClass('showTab');
            $('#search-box').val("");
            document.getElementById('search-close').classList.add('hidden');
            $('#region-select input').removeClass('active');
            $(this).addClass('active');
            if ($(this).val() == "Greater Melbourne") {
                gtag('event', 'click', {
                    'event_category': 'tab',
                    'event_label': 'Greater Melbourne tab'
                });

                regional = false;
                map.fitBounds(normalBounds);
            } else {
                gtag('event', 'click', {
                    'event_category': 'tab',
                    'event_label': 'Regional tab'
                });

                regional = true;
                map.fitBounds(regionalBounds);
            }
            map.data.revertStyle();
            if ($('.custom-switch')[0].checked) {
                if (custom_complete) {
                    customMapUpdate();
                } else {
                    custom_clear();
                }
            } else {
                clearMap();
            }
        }
    });

    $('.q-slider').on("slidechange", function (event, ui) {
        if (custom_complete) {
            customMapUpdate();
        }
    });

    $('#search-box').on('focus', function () {
        gtag('event', 'type', {
            'event_category': 'Search Bar',
            'event_label': 'CityPulse Melbourne'
        });
    });

    // Search autocomplete
    $(function () {
        $('#search-box').autocomplete({
            minLength: 1,
            delay: 500,
            source: suburb_list,
            messages: {
                noResults: '',
                results: function () { }
            },
            select: function (event, ui) {
                $("#search-box").val(ui.item.label);
                if (isMobile) {
                    $('#search-box').toggleClass('open');
                    $('#search-box').trigger('blur');
                }
                searching = false;
                if (ui.item.type == "Postcode") {
                    var loc = ui.item.suburb
                } else {
                    var loc = ui.item.label
                }

                searchSelect = loc.toLowerCase();
                getSearchItems(loc.toLowerCase(), true);
                if (isTouch) { openInfoBox(loc) };
                return false;
            },
            create: function () {
                $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                    if (allRegionalZones.indexOf(item.label) > -1 && regional || allRegionalZones.indexOf(item.label) < 0 && !regional) {
                        if ((item.pp >= minValue && item.pp <= maxValue) || ($('#ins-data')[0].checked && item.pp == 0)) {
                            if (item.desc == "Statistical Area 2 (ABS)") {
                                return $("<li>")
                                    .append("<div><span class='label'>" + item.label + "</span><br><span class='sch-type'>" + item.desc + "</span><div class='search-arrow'></div></div>")
                                    .appendTo(ul);
                            } else {
                                if (item.type == "Postcode") {
                                    return $("<li>")
                                        .append("<div><span class='label'>" + item.suburb + ", " + item.pcode + "</span><br><span class='sch-type'>" + item.desc + "</span><div class='search-arrow'></div></div>")
                                        .appendTo(ul);
                                } else {
                                    return $("<li>")
                                        .append("<div><span class='label'>" + item.label + ", " + item.pcode + "</span><br><span class='sch-type'>" + item.desc + "</span><div class='search-arrow'></div></div>")
                                        .appendTo(ul);
                                }
                            }
                        } else {
                            return $("<li class='hidden'>")
                                .append("<div><span class='label'>" + item.label + ", " + item.pcode + "</span><br><span class='sch-type'>" + item.desc + "</span><div class='search-arrow'></div></div>")
                                .appendTo(ul);
                        }
                    } else {
                        return $("<li class='hidden'>")
                            .append("<div><span class='label'>" + item.label + ", " + item.pcode + "</span><br><span class='sch-type'>" + item.desc + "</span><div class='search-arrow'></div></div>")
                            .appendTo(ul);
                    }
                }
            },
            open: function (event, ui) {
                if (isMobile) {
                    $('.ui-autocomplete').off('menufocus hover mouseover mouseenter');
                }
            },
            response: function () {
                dynamicMapSearch();
                return false;
            }
        });
        $('#search-box').attr("placeholder", "Search by suburb or postcode...");
    });

    // Add completion ticks after questions answered
    $('#qs-content input').on('click', function () {
        var id = $(this)[0].name;
        switch (id) {
            case 'lifestage-option':
                if (qs_answered.indexOf(id) < 0) {
                    qs_answered.push(id);
                    $(this)[0].parentNode.previousElementSibling.classList.add('completed');
                }
                break;
            case 'tp-owork-option':
                if (qs_answered.indexOf(id) < 0) {
                    qs_answered.push(id);
                    if (qs_answered.indexOf('tp-work-option') > -1) {
                        $(this)[0].parentNode.parentNode.parentNode.previousElementSibling.classList.add('completed');
                    }
                }
                break;
            case 'tp-work-option':
                if (qs_answered.indexOf(id) < 0) {
                    qs_answered.push(id);
                    if (qs_answered.indexOf('tp-owork-option') > -1) {
                        $(this)[0].parentNode.parentNode.parentNode.previousElementSibling.classList.add('completed');
                    }
                }
                break;
            default:
                break;
        }
        if (qs_answered.length == 3) {
            $('.panel.activity')[0].previousElementSibling.classList.add('completed');
            custom_complete = true;
        }

        if (custom_complete) {
            customMapUpdate();
        }
    });

    map.data.addListener('mouseout', function (event) {
        $('.popup-tip-anchor').addClass('hidden');
        map.data.revertStyle();
    });

    map.addListener('click', function (e) {
        if ($('#qs-tab')[0].classList.contains('showTab')) {
            $('#qs-tab').removeClass('showTab');
        }
        if (isMobile && infowindow.getMap() != null) {
            infowindow.close();
        }
    });

    function populateMobilePopup(feature) {
        var capName = mapType.charAt(0).toUpperCase() + mapType.slice(1);
        var myHTML = "<div id='popup-info'><p style='font-weight:bold;margin: 0px 0px 10px 0px !important;'>";
        myHTML = myHTML + feature.getProperty("name");
        myHTML = myHTML + "</p>";

        if (custom_complete && $('#qs-tab')[0].classList.contains('custom-on')) {
            var value = parseFloat(feature.getProperty("c_" + mapType)).toFixed(1);
            info_values['live'] = parseFloat(feature.getProperty("c_live")).toFixed(1);
            info_values['work'] = parseFloat(feature.getProperty("c_work")).toFixed(1);
            info_values['play'] = parseFloat(feature.getProperty("c_play")).toFixed(1);
        } else {
            var value = parseFloat(feature.getProperty(mapType));
            info_values['live'] = parseFloat(feature.getProperty('live'));
            info_values['work'] = parseFloat(feature.getProperty('work'));
            info_values['play'] = parseFloat(feature.getProperty('play'));
        }

        if (!isNaN(value)) {
            if (!custom_complete && $('#qs-tab')[0].classList.contains('custom-on')) {
                myHTML = myHTML + "<table width='100%'><tbody><tr><td width='50%' style='padding-right:8px'>" + capName + "</td><td width='50%' style='text-align:center;'>-</td></tr>";
            } else {
                if (value >= 1) {
                    myHTML = myHTML + "<table width='100%'><tbody><tr><td width='50%' style='padding-right:8px'>" + capName + "</td><td width='50%' style='text-align:center;'>" + value + "</td></tr>";
                } else {
                    myHTML = myHTML + "<table width='100%'><tbody><tr><td width='50%' style='padding-right:8px'>" + capName + "</td><td width='50%' style='text-align:center;'>&lt;1</td></tr>";
                }
            }
        } else {
            myHTML = myHTML + "<table width='100%'><tbody><tr><td width='100%'>Low population</td></tr>";
        }
        if ($('#qs-tab')[0].classList.contains('custom-on')) {
            myHTML = myHTML + "<tr><td width='75%'>Custom Average</td><td width='25%'>" + custom_averages[mapType] + "</td></tr>"
        } else {
            if (regional) {
                myHTML = myHTML + "<tr><td width='75%'>City Average</td><td width='25%'>" + regional_values[mapType] + "</td></tr>"
            } else {
                myHTML = myHTML + "<tr><td width='75%'>City Average</td><td width='25%'>" + average_values[mapType] + "</td></tr>"
            }
        }
        myHTML = myHTML + "</tbody></table>";

        return myHTML;
    }

    if (isTouch) {
        map.data.addListener('click', function (event) {
            if ($('#qs-tab')[0].classList.contains('showTab')) {
                $('#qs-tab').removeClass('showTab');
            }
            infowindow.close();
            info_title = event.feature.getProperty("name");
            var propertyPrice = event.feature.getProperty('property_price');
            var featureName = event.feature.getProperty('name');
            if (allRegionalZones.indexOf(featureName) > -1 && regional || allRegionalZones.indexOf(featureName) < 0 && !regional) {
                if ((propertyPrice >= minValue && propertyPrice <= maxValue) || ($('#ins-data')[0].checked && propertyPrice == 0)) {
                    var html = populateMobilePopup(event.feature);

                    infowindow.setContent(html);
                    infowindow.setPosition(event.latLng);
                    infowindow.open(map);

                    map.data.revertStyle();
                    map.data.overrideStyle(event.feature, {
                        fillOpacity: 1,
                        strokeWeight: 2
                    });
                }
            }
            info_open = true;
        });
    } else if (!isMobile) {
        map.data.addListener('click', function (event) {
            if ($('#qs-tab')[0].classList.contains('showTab')) {
                $('#qs-tab').removeClass('showTab');
            }
            var propertyPrice = event.feature.getProperty('property_price');
            var point = overlay.getProjection().fromLatLngToDivPixel(event.latLng);
            if ((propertyPrice >= minValue && propertyPrice <= maxValue) || ($('#ins-data')[0].checked && propertyPrice == 0)) {
                $('.popup-tip-anchor').css('left', point.x);
                $('.popup-tip-anchor').css('top', point.y);
                $('.popup-tip-anchor').removeClass('hidden');
                $('.popup-content').removeClass('hidden');
                $('.popup-bubble-anchor').removeClass('hidden');
            }
        });
        map.data.addListener('mouseover', function (event) {
            var propertyPrice = event.feature.getProperty('property_price');
            var featureName = event.feature.getProperty('name');
            var point = overlay.getProjection().fromLatLngToDivPixel(event.latLng);
            if (allRegionalZones.indexOf(featureName) > -1 && regional || allRegionalZones.indexOf(featureName) < 0 && !regional) {
                if ((propertyPrice >= minValue && propertyPrice <= maxValue) || ($('#ins-data')[0].checked && propertyPrice == 0)) {
                    var capName = mapType.charAt(0).toUpperCase() + mapType.slice(1);
                    var myHTML = "<table width='100%'><tbody><tr><td style='font-weight:bold;margin:0px !important;padding-bottom:10px;' colspan='2'>";
                    myHTML = myHTML + event.feature.getProperty("name");
                    myHTML = myHTML + "</td></tr>";
                    $('.popup-tip-anchor').css('width', '150px');
                    $('.popup-tip-anchor').css('left', point.x);
                    $('.popup-tip-anchor').css('top', point.y);

                    if (custom_complete && $('#qs-tab')[0].classList.contains('custom-on')) {
                        var value = parseFloat(event.feature.getProperty("c_" + mapType)).toFixed(1);
                    } else {
                        var value = parseFloat(event.feature.getProperty(mapType));
                    }

                    if (!isNaN(value)) {
                        if (!custom_complete && $('#qs-tab')[0].classList.contains('custom-on')) {
                            myHTML = myHTML + "<tr><td width='25%''>" + capName + "</td><td width='25%'>-</td></tr>";
                        } else {
                            if (value >= 1) {
                                myHTML = myHTML + "<tr><td width='25%''>" + capName + "</td><td width='25%'>" + value + "</td></tr>";
                            } else {
                                myHTML = myHTML + "<tr><td width='25%''>" + capName + "</td><td width='25%'>" + "&lt;1</td></tr>";
                            }
                        }
                    } else {
                        myHTML = myHTML + "<tr><td>Low population</td></tr>";
                    }
                    if ($('#qs-tab')[0].classList.contains('custom-on')) {
                        myHTML = myHTML + "<tr><td width='75%'>Custom Average</td><td width='25%'>" + custom_averages[mapType] + "</td></tr>"
                    } else {
                        if (regional) {
                            myHTML = myHTML + "<tr><td width='75%'>City Average</td><td width='25%'>" + regional_values[mapType] + "</td></tr>"
                        } else {
                            myHTML = myHTML + "<tr><td width='75%'>City Average</td><td width='25%'>" + average_values[mapType] + "</td></tr>"
                        }
                    }
                    myHTML = myHTML + "</tbody></table>";

                    $('.popup-content')[0].innerHTML = myHTML;
                    $('.popup-tip-anchor').removeClass('hidden');
                    $('.popup-content').removeClass('hidden');
                    $('.popup-bubble-anchor').removeClass('hidden');

                    map.data.revertStyle();
                    map.data.overrideStyle(event.feature, {
                        fillOpacity: 1,
                        strokeWeight: 2
                    });
                }
            }
        });
    }

    $('#ins-data').on('change', function () {
        // styleGEOJson(mapType);
        clearMap();
        var searchTerm = $('#search-box').val().toLowerCase();
        getSearchItems(searchTerm, !(searching && searchTerm.length > 1));
    });

    $('#search-box').on('keyup input', function () {
        var searchTerm = $(this).val().toLowerCase();
        if (searchTerm.length == 0) {
            clearMap();
            document.getElementById('search-close').classList.add('hidden');
        } else {
            document.getElementById('search-close').classList.remove('hidden');
        }
    });

    $('.search-close').on('click', function () {
        if (isTouch) {
            infowindow.close();
            map.data.revertStyle();
        }
        searchSelect = '';
        $(this).addClass('hidden');
        $('#search-box').val("");
        clearMap();
        (regional) ? map.fitBounds(regionalBounds) : map.fitBounds(normalBounds);
    });

    function sendSliderInfo() {
        if (minValue == maxValue) {
            gtag('event', 'select', {
                'event_category': 'slider',
                'event_label': minValue + ' budget only'
            });
        } else {
            gtag('event', 'select', {
                'event_category': 'slider',
                'event_label': minValue + ' min and ' + maxValue + ' max budget'
            });
        }
    }

    $("#slider-range").on('slide', function (event, ui) {
        clearTimeout(sliderTimer);
        sliderTimer = setTimeout(sendSliderInfo, 2000);


        if (isTouch) {
            infowindow.close();
        }
        if ($('#search-box').val().length < 1) {
            map.data.revertStyle();
            map.data.setStyle(function (feature) {
                var featureName = feature.getProperty('name');
                if (allRegionalZones.indexOf(featureName) > -1 && regional || allRegionalZones.indexOf(featureName) < 0 && !regional) {
                    if (custom_complete && $('#qs-tab')[0].classList.contains('custom-on')) {
                        var color = selectColour(feature.getProperty("c_" + mapType));
                    } else {
                        var color = selectColour(parseFloat(feature.getProperty(mapType)));
                    }
                    var propertyPrice = feature.getProperty('property_price');
                    if ((propertyPrice >= minValue && propertyPrice <= maxValue) || ($('#ins-data')[0].checked && propertyPrice == 0)) {
                        return {
                            fillColor: color,
                            fillOpacity: 0.5,
                            strokeWeight: 0.5,
                            strokeColor: '#6D6E71'
                        }
                    } else {
                        return {
                            fillOpacity: 0,
                            strokeWeight: 0,
                        };
                    }
                } else {
                    return {
                        fillOpacity: 0,
                        strokeWeight: 0,
                    }
                }
            });
        } else {
            var searchTerm = $('#search-box').val().toLowerCase();
            map.data.setStyle(function (feature) {
                var featureName = feature.getProperty('name');
                if (allRegionalZones.indexOf(featureName) > -1 && regional || allRegionalZones.indexOf(featureName) < 0 && !regional) {
                    if (custom_complete && $('#qs-tab')[0].classList.contains('custom-on')) {
                        var color = selectColour(feature.getProperty("c_" + mapType));
                    } else {
                        var color = selectColour(parseFloat(feature.getProperty(mapType)));
                    }
                    var propertyPrice = feature.getProperty('property_price');
                    if ((propertyPrice >= minValue && propertyPrice <= maxValue) || ($('#ins-data')[0].checked && propertyPrice == 0)) {
                        if (searchSelect) {
                            if (feature.getProperty('name').toLowerCase() == searchSelect) {
                                return {
                                    fillColor: color,
                                    fillOpacity: 0.5,
                                    strokeWeight: 2,
                                    strokeColor: '#6D6E71'
                                };
                            } else {
                                return {
                                    fillColor: color,
                                    fillOpacity: 0.05,
                                    strokeWeight: 0.5,
                                    strokeColor: '#6D6E71'
                                };
                            }
                        } else if (feature.getProperty('name').toLowerCase().indexOf(searchTerm) >= 0 || feature.getProperty('suburbs').toLowerCase().split(';').indexOf(searchTerm) >= 0 || feature.getProperty('postcodes').toString().split(';').indexOf(searchTerm) >= 0) {
                            return {
                                fillColor: color,
                                fillOpacity: 0.5,
                                strokeWeight: 2,
                                strokeColor: '#6D6E71'
                            };
                        } else {
                            return {
                                fillColor: color,
                                fillOpacity: 0.05,
                                strokeWeight: 0.5,
                                strokeColor: '#6D6E71'
                            };
                        }
                    } else {
                        return {
                            fillOpacity: 0,
                            strokeWeight: 0,
                        }
                    }
                } else {
                    return {
                        fillOpacity: 0,
                        strokeWeight: 0,
                    }
                }
            });
        }
    });
}

// Custom popup functions
function definePopupClass() {
    Popup = function (position, content) {
        this.position = position;

        var pixelOffset = document.createElement('div');
        pixelOffset.classList.add('popup-bubble-anchor');
        pixelOffset.classList.add('hidden');
        pixelOffset.appendChild(content);

        this.anchor = document.createElement('div');
        this.anchor.classList.add('popup-tip-anchor');
        this.anchor.appendChild(pixelOffset);
        this.stopEventPropagation();
    };

    Popup.prototype = Object.create(google.maps.OverlayView.prototype);

    Popup.prototype.onAdd = function () {
        this.getPanes().floatPane.appendChild(this.anchor);
    };

    Popup.prototype.onRemove = function () {
        if (this.anchor.parentElement) {
            this.anchor.parentElement.removeChild(this.anchor);
        }
    };

    Popup.prototype.stopEventPropagation = function () {
        var anchor = this.anchor;
        anchor.style.cursor = 'auto';
        ['click', 'dblclick', 'contextmenu', 'wheel', 'mousedown', 'touchstart',
            'pointerdown'
        ]
            .forEach(function (event) {
                anchor.addEventListener(event, function (e) {
                    e.stopPropagation();
                });
            });
    };
}

function processPoints(geometry, callback, thisArg) {
    if (geometry instanceof google.maps.LatLng) {
        callback.call(thisArg, geometry);
    } else if (geometry instanceof google.maps.Data.Point) {
        callback.call(thisArg, geometry.get());
    } else {
        geometry.getArray().forEach(function (g) {
            processPoints(g, callback, thisArg);
        });
    }
}

function kml2gjson_clr(kml_clr) {
    return "#" + kml_clr.substr(6, 2) + kml_clr.substr(4, 2) + kml_clr.substr(2, 2);
}

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};