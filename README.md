# pwc-citypulse-vic-2019

Process to get this to work

1. Open their KML file and globally rename 'Walkability' to 'Play'
2. Run the attached kml-convert.py file (update the filenames in the .PY)
3. Open the new KML file and globally replace "<1.0" with "0"
4. Ensure you have NODE installed
5. Ensure you have 'togeojson' installed (  npm install -g @mapbox/togeojson   )
6. At the command prompt run: togeojson -f kml file-from-kml-convert.kml > output.json
7. Go to https://mapshaper.org/
8. Import the output.json file
9. Open the sites 'console' and enter "-simplify 4%"
10. Export as geoJson to yourfilename.json
11. Upload yourfilename.json to an appropriate scripts folder (/content/pwc/script/au/en/interactive/citypulse)
12. Change the file reference in the Melbourne City Pulse HTML to reference your file:

map.data.loadGeoJson('/content/pwc/script/au/en/interactive/citypulse/mel-2021.json');


Legacy documentation is available here: https://docs.google.com/document/d/1WDoI73fCRTjwhZy_xihveGXdTn4Rrai752FC9kL1Sus/edit?usp=sharing

