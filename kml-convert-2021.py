import xml.etree.ElementTree as ET
import csv

tree = ET.parse('mel-map.kml')
root = tree.getroot()

o = open('colors.csv', 'w', encoding='utf-8')

with open('mel-2021.kml','w') as w:
    w.write('<?xml version="1.0" encoding="utf-8" ?><kml xmlns="http://www.opengis.net/kml/2.2"><Document><Folder><name>Website_map_input_file_20190715</name>')
    for placemark in root.iter('Placemark'):
        # if(placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Pulse city']").text == "Perth"):
        w.write('<Placemark>')
        w.write('<name>' + placemark.find("./ExtendedData/SchemaData/SimpleData[@name='SA2_NAME16']").text + '</name>')
        print(placemark.find("./ExtendedData/SchemaData/SimpleData[@name='SA2_NAME16']").text)
        w.write('<ExtendedData>')
        w.write('<Data name="Pulse city"><value>' + placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Pulse city']").text + '</value></Data>')
        w.write('<Data name="City region"><value>' + placemark.find("./ExtendedData/SchemaData/SimpleData[@name='City region']").text + '</value></Data>')
        w.write('<Data name="live"><value>' + placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Live']").text + '</value></Data>')
        w.write('<Data name="live_clr"><value>' + placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Live_colour_kml']").text + '</value></Data>')
        w.write('<Data name="work"><value>' + placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Work']").text + '</value></Data>')
        w.write('<Data name="work_clr"><value>' + placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Work_colour_kml']").text + '</value></Data>')
        w.write('<Data name="play"><value>' + placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Play']").text + '</value></Data>')
        w.write('<Data name="play_clr"><value>' + placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Play_colour_kml']").text + '</value></Data>')

        o.write('"{0}","{1}"\n'.format("Live", placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Live_colour_kml']").text))
        o.write('"{0}","{1}"\n'.format("Work", placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Work_colour_kml']").text))
        o.write('"{0}","{1}"\n'.format("Play", placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Play_colour_kml']").text))

        # Top 10
        # string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Live_top_10']").text
        # if(string):
        #     w.write('<Data name="live_rank"><value>' + string + '</value></Data>')
        # else:
        #     w.write('<Data name="live_rank"><value></value></Data>')
        
        # string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Work_top_10']").text
        # if(string):
        #     w.write('<Data name="work_rank"><value>' + string + '</value></Data>')
        # else:
        #     w.write('<Data name="work_rank"><value></value></Data>')

        # string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Play_top_10']").text
        # if(string):
        #     w.write('<Data name="play_rank"><value>' + string + '</value></Data>')
        # else:
        #     w.write('<Data name="play_rank"><value></value></Data>')

        # update
        fields = ['Live_commentary', 'Work_commentary', 'Play_commentary']
        for field in fields:
            string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='" + field + "']").text
            if(string):
                w.write('<Data name="' + field.lower() + '"><value>' + string + '</value></Data>')
            else:
                w.write('<Data name="' + field.lower() + '"><value></value></Data>')

        # suburbs
        string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Suburbs']").text
        if (string):
            w.write('<Data name="suburbs"><value>' + string + '</value></Data>')
        else:
            w.write('<Data name="suburbs"><value></value></Data>')

        # postcodes
        #string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Postcodes']").text
        #if (string):
        #    w.write('<Data name="postcodes"><value>' + string + '</value></Data>')
        #else:
        w.write('<Data name="postcodes"><value></value></Data>')

        # property price
        #string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Property price filter quartile']").text
        #if (string):
        #    w.write('<Data name="property_price"><value>' + string + '</value></Data>')
        #else:
        w.write('<Data name="property_price"><value></value></Data>')

        #indicator list
        #string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Indicator_list']").text
        #if (string):
        #    w.write('<Data name="indicator_title"><value>' + string + '</value></Data>')
        #else:
        w.write('<Data name="indicator_title"><value></value></Data>')
        
        #value list
        #string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Value_list']").text
        #if (string):
        #    w.write('<Data name="indicator_values"><value>' + string + '</value></Data>')
        #else:
        w.write('<Data name="indicator_values"><value></value></Data>')

        w.write('</ExtendedData>')
        holder = placemark.find("./Polygon/outerBoundaryIs/LinearRing/coordinates")
        inner = placemark.find("./Polygon/innerBoundaryIs/LinearRing/coordinates")
        if(holder is not None):
            w.write('<Polygon><outerBoundaryIs><LinearRing><coordinates>' + holder.text + '</coordinates></LinearRing></outerBoundaryIs>')
            if (inner is not None):
                w.write('<innerBoundaryIs><LinearRing><coordinates>' + inner.text + '</coordinates></LinearRing></innerBoundaryIs></Polygon>')
            else:
                w.write('</Polygon>')
        else:
            holder = placemark.find("./MultiGeometry/Polygon/outerBoundaryIs/LinearRing/coordinates")
            w.write('<MultiGeometry><Polygon><outerBoundaryIs><LinearRing><coordinates>' + holder.text + '</coordinates></LinearRing></outerBoundaryIs>')
            if (inner is not None):
                w.write('<innerBoundaryIs><LinearRing><coordinates>' + inner.text + '</coordinates></LinearRing></innerBoundaryIs></Polygon></MultiGeometry>')
            else:
                w.write('</Polygon></MultiGeometry>')
        w.write('</Placemark>' + '\n')
    w.write('</Folder></Document></kml>')
            
