import xml.etree.ElementTree as ET
import csv

tree = ET.parse('map2.kml')
root = tree.getroot()

with open('map2-2019.kml','w') as w:
    w.write('<?xml version="1.0" encoding="utf-8" ?><kml xmlns="http://www.opengis.net/kml/2.2"><Document><Folder><name>Website_map_input_file_20190715</name>')
    for placemark in root.iter('Placemark'):
        # if(placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Pulse city']").text == "Perth"):
        w.write('<Placemark>')
        w.write('<name>' + placemark.find("./ExtendedData/SchemaData/SimpleData[@name='SA4_Name']").text + '</name>')
        print(placemark.find("./ExtendedData/SchemaData/SimpleData[@name='SA4_Name']").text)
        w.write('<ExtendedData>')
        w.write('<Data name="Pulse city"><value>' + placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Pulse city']").text + '</value></Data>')

        # LWP
        string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Category']").text
        if (string):
            w.write('<Data name="lwp"><value>' + string + '</value></Data>')
        else:
            w.write('<Data name="lwp"><value></value></Data>')

        string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Score']").text
        if (string):
            w.write('<Data name="score"><value>' + string + '</value></Data>')
        else:
            w.write('<Data name="score"><value></value></Data>')

        # suburbs
        string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Suburb']").text
        if (string):
            w.write('<Data name="suburbs"><value>' + string + '</value></Data>')
        else:
            w.write('<Data name="suburbs"><value></value></Data>')

        # postcodes
        string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Postcode']").text
        if (string):
            w.write('<Data name="postcodes"><value>' + string + '</value></Data>')
        else:
            w.write('<Data name="postcodes"><value></value></Data>')

        # # property price
        # string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Property price filter quartile']").text
        # if (string):
        #     w.write('<Data name="property_price"><value>' + string + '</value></Data>')
        # else:
        #     w.write('<Data name="property_price"><value></value></Data>')

        #indicator list / Domain
        string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Domain']").text
        if (string):
            w.write('<Data name="domain"><value>' + string + '</value></Data>')
        else:
            w.write('<Data name="domain"><value></value></Data>')
        #value list / Difference
        string = placemark.find("./ExtendedData/SchemaData/SimpleData[@name='Difference']").text
        if (string):
            w.write('<Data name="difference"><value>' + string + '</value></Data>')
        else:
            w.write('<Data name="difference"><value></value></Data>')

        w.write('</ExtendedData>')
        holder = placemark.find("./Polygon/outerBoundaryIs/LinearRing/coordinates")
        inner = placemark.find("./Polygon/innerBoundaryIs/LinearRing/coordinates")
        if(holder is not None):
            w.write('<Polygon><outerBoundaryIs><LinearRing><coordinates>' + holder.text + '</coordinates></LinearRing></outerBoundaryIs>')
            if (inner is not None):
                w.write('<innerBoundaryIs><LinearRing><coordinates>' + inner.text + '</coordinates></LinearRing></innerBoundaryIs></Polygon>')
            else:
                w.write('</Polygon>')
        else:
            holder = placemark.find("./MultiGeometry/Polygon/outerBoundaryIs/LinearRing/coordinates")
            w.write('<MultiGeometry><Polygon><outerBoundaryIs><LinearRing><coordinates>' + holder.text + '</coordinates></LinearRing></outerBoundaryIs>')
            if (inner is not None):
                w.write('<innerBoundaryIs><LinearRing><coordinates>' + inner.text + '</coordinates></LinearRing></innerBoundaryIs></Polygon></MultiGeometry>')
            else:
                w.write('</Polygon></MultiGeometry>')
        w.write('</Placemark>' + '\n')
    w.write('</Folder></Document></kml>')
            
