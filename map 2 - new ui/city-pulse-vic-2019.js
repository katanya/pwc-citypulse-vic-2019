// Copyright © 2018 Katanya LLP (UK) [www.katanya.co.uk].  All rights reserved. //
var isMobile = false;
var isTouch = false;
var featureCount = 0;
var minValue = 1;
var maxValue = 4;

var map, popup, Popup;
var mapType = 'perception';
var domainVal = 0;
var domainHeading = '';
var domainAverages = [8.09, 7.64, 7.27, 7.18, 7.07, 6.87, 6.77, 6.74, 6.02, 5.89]
var differences = [];
var defaultLWP = ['Live', 'Work', 'Play'];

var searching = false;
var colourScale = ['#fff0e8','#fedece','#fed0b8','#fec5a7','#feb994','#feab7f','#fea271','#fd9760','#fd8748','#fd742a','#fd6412']
var colourMap = {
    "00ffffff": "00ffffff",
    "ff1a98ed": "ff1e30e0",
    "ff80c6f5": "ff0f73f0",
    "ffeaeffc": "ff00b6ff",
    "ffaa9cec": "ffd9c97d",
    "ff654ddd": "fff9dcb3"
}

var infowindow;
var info_open = false;
var info_title;
var info_values = [];

// Variables related to the map
var all_centers = [];
var markers = [];
var suburb_list = [];
var filtered_suburbs = [];

$(window).on('load resize', function () {
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
        isTouch = true;
        if ($(window).width() < 650) {
            isMobile = true;
        } else {
            isMobile = false;
        }
    } else {
        isMobile = false;
        isTouch = false;
    }
}).resize();

$(document).bind('mousemove', function (e) {
    $('.popup-tip-anchor').offset({
        left: e.pageX + 20,
        top: e.pageY + 20
    });
});


// ----------------------- FUNCTIONS ----------------------- //
function selectColour(score) {
    if (score === "N/A" || isNaN(score)) {
        return "#ffffff";
    } else {
        var s = (differences.indexOf(score) / differences.length) * 10;
        var percentile = Math.floor(s);
        return colourScale[percentile];
    }
}

function styleGEOJson() {
    map.data.setStyle(function (feature) {
        var color = selectColour(feature.getProperty('differenceList')[domainVal]);
        return {
            fillColor: color,
            fillOpacity: 0.5,
            strokeWeight: 0.5,
            strokeColor: '#6D6E71'
        };
    });
}

function addSuburbs(suburb, sa4, postcode) {
    var suburbs = suburb.split(";");
    var postcodes = postcode.toString().split(";");
    for (var item in suburbs) {
        if (suburbs[item].length > 0) {
            if (filtered_suburbs.indexOf(suburbs[item]) < 0) {
                filtered_suburbs.push(suburbs[item]);
                var sb_key = suburbs[item];
                var data = {
                    label: sb_key,
                    type: 'Suburb',
                    desc: 'Suburb',
                    pcode: postcodes[item]
                }
                var pdata = {
                    label: postcodes[item],
                    type: 'Postcode',
                    suburb: sb_key,
                    desc: 'Suburb',
                    pcode: postcodes[item]
                }
                suburb_list.push(data);
                suburb_list.push(pdata);
            }
        }
    }
    if (filtered_suburbs.indexOf(sa4) < 0) {
        filtered_suburbs.push(sa4);
        var data = {
            label: sa4,
            type: 'suburb',
            desc: 'Statistical Area 4 (ABS)',
            pcode: postcode
        }
        suburb_list.push(data);
    }
    suburb_list.sort();
}

function getPolygonCenters(event, list) {
    var marker_bounds = new google.maps.LatLngBounds();
    event.feature.getGeometry().forEachLatLng(function (latlng) {
        marker_bounds.extend(latlng);
    });
    list.push(marker_bounds);
}

var styles = {
    default: null,
    hide: [
        {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.neighborhood",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.local",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        }
    ]
};

function initMap() {
    var bounds = new google.maps.LatLngBounds();
    // Setup the map and related features
    if (isTouch) {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            mapTypeControl: false,
            streetViewControl: false,
            zoomControl: false,
            fullscreenControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            },
            center: {
                lat: -37.8,
                lng: 144.9
            }
        });
    } else {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            mapTypeControl: false,
            streetViewControl: false,
            fullscreenControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            },
            center: {
                lat: -37.8,
                lng: 144.9
            }
        });
    }
    map.setOptions({ styles: styles['hide'] });
    map.data.loadGeoJson('city-pulse-vic-2019.json');
    if (isTouch) {
        infowindow = new google.maps.InfoWindow({
            pixelOffset: new google.maps.Size(0, -10)
        });
    }
    autoSelect(0);
    styleGEOJson();

    map.data.addListener('addfeature', function (event) {
        featureCount++;
        event.feature.setProperty('id', featureCount);
        var l = event.feature.getProperty('difference').split(';');
        var newList = [];
        for (var p = 0; p < l.length; p++) {
            newList.push(parseFloat(l[p]));
        }
        event.feature.setProperty('differenceList', newList);
        differences = differences.concat(event.feature.getProperty('differenceList'));
        differences.sort((a, b) => a - b);
        processPoints(event.feature.getGeometry(), bounds.extend, bounds);
        getPolygonCenters(event, all_centers);
        addSuburbs(event.feature.getProperty('suburbs'), event.feature.getProperty('name'), event.feature.getProperty('postcodes'));
        if (!isMobile) {
            map.fitBounds(bounds);
        }
    });

    definePopupClass();
    popup = new Popup(
        new google.maps.LatLng(-33, 151),
        document.getElementById('content'));
    popup.setMap(map);

    var overlay = new google.maps.OverlayView();
    overlay.draw = function () { };
    overlay.setMap(map);

    // Add controls to the map
    var perceptionDiv = document.getElementById('perception-box');
    var infrastructureDiv = document.getElementById('infrastructure-box');
    var boxContainerDiv = document.getElementById('box-container');
    var keyDiv = document.getElementById('key-box');

    var searchControl = document.getElementById('area-search');
    perceptionDiv.index = 1;

    if (isMobile) {
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(searchControl);
        map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(perceptionDiv);
        map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(infrastructureDiv);
        map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(keyDiv);
    } else {
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(boxContainerDiv);
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(searchControl);
        map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(keyDiv);
    }

    // ------------------- FUNCTIONS ----------------------------- //
    function clearMap() {
        styleGEOJson();
        // map.fitBounds(bounds);
        if (isMobile) {
            map.setZoom(7);
        }
    }

    function autoSelect(num) {
        $('.mapTypes')[0].className += " selected";
        $('.grid-square')[num].className += " active";
        $('.domain-heading')[0].id = "num" + num;
        $('.domain-heading')[0].innerText = "I Love Where I Live";
        domainHeading = "I Love Where I Live";
        domainVal = 0;
    }

    $('.key-tab').on('click', function(e) {
        $('#key-box').toggleClass('hide');
    });

    $('.grid-square').on('mouseover', function(e) {
        $('.domain-heading')[0].innerText = this.dataset.value;
    });

    $('.grid-square').on('mouseleave', function(e) {
        $('.domain-heading')[0].innerText = domainHeading;
    });

    $('.grid-square').on('click', function(e) {
        domainHeading = this.dataset.value;
        domainVal = this.dataset.num;

        $('.grid-square').removeClass('active');
        $(this).addClass('active');

        $('.domain-heading')[0].innerText = domainHeading;
        $('.domain-heading')[0].id = "num" + domainVal;

        clearMap();

        var searchTerm = $('#search-box').val().toLowerCase();
        getSearchItems(searchTerm, !(searching && searchTerm.length > 1));
    });

    function getSearchItems(searchTerm, click) {
        if (searchTerm === "") {
            styleGEOJson();
            document.getElementById('search-close').classList.add('hidden');
        } else {
            var search_bounds = new google.maps.LatLngBounds();
            var searchAreas = [];
            var searchFound = false;
            map.data.forEach(function (feature) {
                var featureSuburbs = feature.getProperty('suburbs').toLowerCase().split(';');
                var featurePostcodes = feature.getProperty('postcodes').toString().split(',');
                if (feature.getProperty('name').toLowerCase() === searchTerm) {
                    searchAreas.push(feature.getProperty('name').toLowerCase());
                } else {
                    if (featureSuburbs.length > 0 && searchAreas.length == 0) {
                        for (var item in featureSuburbs) {
                            if (featureSuburbs[item] === searchTerm) {
                                searchAreas.push(feature.getProperty('name').toLowerCase());
                            }
                        }
                    }
                    if (featurePostcodes.length > 0 && searchAreas.length == 0) {
                        for (var item in featurePostcodes) {
                            if (featurePostcodes[item] === searchTerm) {
                                searchAreas.push(feature.getProperty('name').toLowerCase());
                            }
                        }
                    }
                }
            });
            map.data.forEach(function (feature) {
                if (!click) {
                    var condition = (feature.getProperty('name').toLowerCase().indexOf(searchTerm) >= 0 || feature.getProperty('suburbs').toLowerCase().indexOf(searchTerm) >= 0 || feature.getProperty('postcodes').toString().indexOf(searchTerm) >= 0);
                } else {
                    var condition = (searchAreas.indexOf(feature.getProperty('name').toLowerCase()) >= 0);
                }
                if (condition) {
                    processPoints(feature.getGeometry(), search_bounds.extend, search_bounds);
                    map.fitBounds(search_bounds);
                    searchFound = true;
                }
                if (!searchFound) {
                    map.fitBounds(bounds);
                }
            });

            map.data.setStyle(function (feature) {
                var color = selectColour(feature.getProperty('differenceList')[domainVal]);
                if (!click) {
                    var condition = (feature.getProperty('name').toLowerCase().indexOf(searchTerm) >= 0 || feature.getProperty('suburbs').toLowerCase().indexOf(searchTerm) >= 0 || feature.getProperty('postcodes').toString().indexOf(searchTerm) >= 0);
                } else {
                    var condition = (searchAreas.indexOf(feature.getProperty('name').toLowerCase()) >= 0);
                }

                if (condition) {
                    return {
                        fillColor: color,
                        fillOpacity: 0.5,
                        strokeWeight: 2,
                        strokeColor: '#6D6E71'
                    };
                } else {
                    return {
                        fillColor: color,
                        fillOpacity: 0.05,
                        strokeWeight: 0.5,
                        strokeColor: '#6D6E71'
                    };
                }
            });
        }
    }

    function dynamicMapSearch() {
        var searchTerm = $('#search-box').val().toLowerCase();
        if (searchTerm.length > 0) {
            searching = true;
            getSearchItems(searchTerm, false);
        } else if (searchTerm.length == 0) {
            clearMap();
        }
    }

    // Search autocomplete
    $(function () {
        $('#search-box').autocomplete({
            minLength: 1,
            delay: 500,
            source: suburb_list,
            messages: {
                noResults: '',
                results: function () { }
            },
            select: function (event, ui) {
                $("#search-box").val(ui.item.label);
                if (isMobile) {
                    $('#search-box').toggleClass('open');
                    document.getElementById('search-close').classList.add('hidden');
                    $('#search-box').trigger('blur');
                }
                searching = false;
                if (ui.item.type == "Postcode") {
                    var loc = ui.item.suburb
                } else {
                    var loc = ui.item.label
                }
                getSearchItems(loc.toLowerCase(), true);
                return false;
            },
            create: function () {
                $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                    if (item.desc == "Statistical Area 2 (ABS)") {
                        return $("<li>")
                            .append("<div><span class='label'>" + item.label + "</span><br><span class='sch-type'>" + item.desc + "</span></div>")
                            .appendTo(ul);
                    } else {
                        if (item.type == "Postcode") {
                            return $("<li>")
                                .append("<div><span class='label'>" + item.suburb + ", " + item.pcode + "</span><br><span class='sch-type'>" + item.desc + "</span></div>")
                                .appendTo(ul);
                        } else {
                            return $("<li>")
                                .append("<div><span class='label'>" + item.label + "," + item.pcode + "</span><br><span class='sch-type'>" + item.desc + "</span></div>")
                                .appendTo(ul);
                        }
                    }
                }
            },
            open: function (event, ui) {
                if (isMobile) {
                    $('.ui-autocomplete').off('menufocus hover mouseover mouseenter');
                }
            },
            response: function () {
                dynamicMapSearch();
                return false;
            }
        });
        $('#search-box').attr("placeholder", "Search by suburb or postcode...");
    });

    map.data.addListener('mouseout', function (event) {
        $('.popup-tip-anchor').addClass('hidden');
        map.data.revertStyle();
    });

    map.addListener('click', function (e) {
        if (isMobile && infowindow.getMap() != null) {
            infowindow.close();
        }
    });

    if (isTouch) {
        map.data.addListener('click', function (event) {
            infowindow.close();
            info_title = event.feature.getProperty("name");
            var myHTML = "<div id='popup-info'><p style='font-weight:bold;margin: 0px 0px 10px 0px !important;'>";
            myHTML = myHTML + event.feature.getProperty("name");
            myHTML = myHTML + "</p>";

            var value = parseFloat(event.feature.getProperty('differenceList')[domainVal]);
            value = value.toFixed(2);
            var multiply = 100 + parseFloat(value);

            if (!isNaN(value)) {
                myHTML = myHTML + "<table width='100%'><tbody><tr><td>" + ((domainAverages[domainVal] * multiply) / 10).toFixed(2) + "% satisfied</td></tr>";
                if(value > 0){
                    myHTML = myHTML + "<tr><td width='25%'>(" + value + "% <span class=\"cp-success\">above</span> average)</td></tr>";
                } else if(value < 0){
                    myHTML = myHTML + "<tr><td width='25%'>(" + value + "% <span class=\"cp-danger\">below</span> average)</td></tr>";
                } else {
                    myHTML = myHTML + "<tr><td width='25%'>(on average)</td></tr>";
                }
            } else {
                myHTML = myHTML + "<p>Low population</p>";
            }
            myHTML = myHTML + "</tbody></table>";

            infowindow.setContent(myHTML);
            infowindow.setPosition(event.latLng);
            infowindow.open(map);

            map.data.revertStyle();
            map.data.overrideStyle(event.feature, {
                fillOpacity: 1,
                strokeWeight: 2
            });
            info_open = true;
        });
    } else if (!isMobile) {
        map.data.addListener('mouseover', function (event) {
            var LWP = event.feature.getProperty('score').split(';');
            var Labels = event.feature.getProperty('lwp').split(';');

            $('.lwp-zone')[0].innerText = event.feature.getProperty("name");

            $('.lwp-no-data').addClass('hidden');
            $('.lwp-label').addClass('hidden');
            $('.lwp-zone').removeClass('hidden');
            $('.lwp-table').removeClass('hidden');

            if (Labels.length == 1) {
                $('.lwp-table').addClass('hidden');
                $('.lwp-label')[0].innerText = 'Unavailable due to low population.';
                $('.lwp-label').removeClass('hidden');
            } else {
                for (var i = 0; i < Labels.length; i++) {
                    $('.lwp-table .' + Labels[i].toLowerCase() + '-score')[0].innerText = Math.round(LWP[i] * 100) / 100;
                }
            }

            var point = overlay.getProjection().fromLatLngToDivPixel(event.latLng);
            var myHTML = "<table width='100%'><tbody><tr><td style='font-weight:bold;margin:0px !important;padding-bottom:10px;' colspan='2'>";
            myHTML = myHTML + event.feature.getProperty("name");
            myHTML = myHTML + "</td></tr>";
            $('.popup-tip-anchor').css('width', '175px');
            $('.popup-tip-anchor').css('left', point.x);
            $('.popup-tip-anchor').css('top', point.y);

            var value = parseFloat(event.feature.getProperty('differenceList')[domainVal]);
            value = value.toFixed(2);
            var multiply = 100 + parseFloat(value);

            if (!isNaN(value)) {
                myHTML = myHTML + "<tr><td>" + ((domainAverages[domainVal] * multiply) / 10).toFixed(2) + "% satisfied</td></tr>";
                if(value > 0){
                    myHTML = myHTML + "<tr><td width='25%'>(" + value + "% <span class=\"cp-success\">above</span> average)</td></tr>";
                } else if(value < 0){
                    myHTML = myHTML + "<tr><td width='25%'>(" + value + "% <span class=\"cp-danger\">below</span> average)</td></tr>";
                } else {
                    myHTML = myHTML + "<tr><td width='25%'>(on average)</td></tr>";
                }
            } else {
                myHTML = myHTML + "<tr><td>Low population</td></tr>";
            }
            myHTML = myHTML + "</tbody></table>";

            $('.popup-content')[0].innerHTML = myHTML;
            $('.popup-tip-anchor').removeClass('hidden');
            $('.popup-content').removeClass('hidden');
            $('.popup-bubble-anchor').removeClass('hidden');

            map.data.revertStyle();
            map.data.overrideStyle(event.feature, {
                fillOpacity: 1,
                strokeWeight: 2
            });
        });
    }

    $('#search-box').on('keyup input', function () {
        var searchTerm = $(this).val().toLowerCase();
        if (searchTerm.length == 0) {
            clearMap();
            document.getElementById('search-close').classList.add('hidden');
        } else {
            document.getElementById('search-close').classList.remove('hidden');
        }
    });

    $('.search-close').on('click', function () {
        $(this).addClass('hidden');
        $('#search-box').val("");
        clearMap();
        map.fitBounds(bounds);
    });
}

// Custom popup functions
function definePopupClass() {
    Popup = function (position, content) {
        this.position = position;

        var pixelOffset = document.createElement('div');
        pixelOffset.classList.add('popup-bubble-anchor');
        pixelOffset.classList.add('hidden');
        pixelOffset.appendChild(content);

        this.anchor = document.createElement('div');
        this.anchor.classList.add('popup-tip-anchor');
        this.anchor.appendChild(pixelOffset);
        this.stopEventPropagation();
    };

    Popup.prototype = Object.create(google.maps.OverlayView.prototype);

    Popup.prototype.onAdd = function () {
        this.getPanes().floatPane.appendChild(this.anchor);
    };

    Popup.prototype.onRemove = function () {
        if (this.anchor.parentElement) {
            this.anchor.parentElement.removeChild(this.anchor);
        }
    };

    Popup.prototype.stopEventPropagation = function () {
        var anchor = this.anchor;
        anchor.style.cursor = 'auto';
        ['click', 'dblclick', 'contextmenu', 'wheel', 'mousedown', 'touchstart',
            'pointerdown'
        ]
            .forEach(function (event) {
                anchor.addEventListener(event, function (e) {
                    e.stopPropagation();
                });
            });
    };
}

function processPoints(geometry, callback, thisArg) {
    if (geometry instanceof google.maps.LatLng) {
        callback.call(thisArg, geometry);
    } else if (geometry instanceof google.maps.Data.Point) {
        callback.call(thisArg, geometry.get());
    } else {
        geometry.getArray().forEach(function (g) {
            processPoints(g, callback, thisArg);
        });
    }
}